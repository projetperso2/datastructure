#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "assert.h"
#include "string.h"
#include "limits.h"

#ifndef A_STRUCTS

#define A_STRUCTS
#define A_NONE 0
#define A_MAX_SIZE_REACH 1
#define A_OUT_OF_BOUND 2
#define A_WILL_OVERFLOW 3

typedef unsigned short A_ERROR;

typedef struct array
{
    int *elements;
    int len;
    int capacity;
    float growth_factor;
} array_t;

#endif

array_t *a_create_complete(int initial_length, float growth_factor)
{
    if(growth_factor <= 1){
        printf("Growth factor must be > 1 \n");
        exit(1);
    }
    array_t *array = (array_t *)malloc(sizeof(array_t));
    if (initial_length > 0)
    {
        array->elements = calloc(initial_length, sizeof(int));
    }
    array->growth_factor = growth_factor;
    array->capacity = initial_length;
    array->len = 0;
    return array;
}

array_t *a_create()
{
    return a_create_complete(5, 1.75);
}

void _a_grow(array_t *array, A_ERROR *returned_error)
{
    if (array->len < array->capacity)
        return;

    int new_len = array->len * array->growth_factor;
    if (array->len > INT_MAX / array->growth_factor)
    {
        *returned_error = A_WILL_OVERFLOW;
        return;
    }
    // allocate a new block of memory
    int *new_elements = (int *)calloc(new_len, sizeof(int));
    // copy old value to new value
    memcpy(new_elements, array->elements, array->capacity);
    // free old memory
    free(array->elements);
    array->elements = new_elements;
    array->capacity = new_len;
}

void a_add(array_t *array, int value, A_ERROR *returned_error)
{
    _a_grow(array, returned_error);
    if (*returned_error != A_NONE)
    {
        fprintf(stderr, "Returned error = %d\n", *returned_error);
        return;
    }
    array->elements[array->len] = value;
    array->len = array->len + 1;
}
void a_set(array_t *array, int index, int value, A_ERROR *returned_error)
{
    if (index >= array->len)
    {
        *returned_error = A_OUT_OF_BOUND;
        return;
    }
}
int a_get(array_t *array, int index, A_ERROR *returned_error)
{
    if (index < 0 || index >= array->len)
    {
        *returned_error = A_OUT_OF_BOUND;
        return 0;
    }
    return array->elements[index];
}

void a_print(array_t *array)
{
    printf("[len=%d, capacity=%d, elements=", array->len, array->capacity);
    for (size_t i = 0; i < array->len; i++)
    {
        printf("%d, ", array->elements[i]);
    }
    printf("]\n");
}

int main(int argc, char const *argv[])
{
    array_t *array = a_create();
    A_ERROR error_code;
    error_code = A_NONE;
    a_add(array, 5, &error_code);
    a_add(array, 4, &error_code);
    a_add(array, 3, &error_code);
    a_add(array, 2, &error_code);
    a_add(array, 1, &error_code);

    a_set(array, 10, 1, &error_code);
    assert(error_code == A_OUT_OF_BOUND);

    a_get(array, 10, &error_code);
    assert(error_code == A_OUT_OF_BOUND);

    a_print(array);
}
