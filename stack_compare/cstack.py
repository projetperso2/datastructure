import time

def main():
    data = []
    start_time = time.time_ns()
    for i in range(0, 10000000):
        data.append(69)
        data.pop()
    end_time = time.time_ns()

    total_time_ns = end_time - start_time
    total_time_us = total_time_ns / 1000
    time_per_operation_us = total_time_us / 10000000

    print(f"Total time: {total_time_us} microseconds")
    print(f"Time per operation: {time_per_operation_us} microseconds")

if __name__ == "__main__":
    main()