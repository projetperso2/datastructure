const process = require('process')

const data = []
const start = process.hrtime.bigint()
for (let index = 0; index < 10000000; index++) {
    data.push(69)
    data.pop()
}
const stop = process.hrtime.bigint()

const totalDuration = (stop - start) / 1000n; // in microseconds
const averageDuration = totalDuration / BigInt(10000000); // in microseconds

console.log(`Total duration: ${totalDuration} microseconds`)
console.log(`Approximate duration of each operation: ${averageDuration} microseconds`)
