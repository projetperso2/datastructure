package stack_compare;
import java.util.Stack;

class cstack {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<Integer>();
        long start = System.nanoTime() / 1000;
        for (int i = 0; i < 10000000; i++) {
            stack.push(69);
            stack.pop();
        }
        long end = System.nanoTime() / 1000;
        System.out.println("Time taken: " + (end - start) + " us");
        System.out.println("Per operation: " + (end - start) / 10000000 + " us");
    }
}