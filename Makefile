CC = gcc
CFLAGS = -std=c11 -pedantic-errors -Wall -Wextra -O3
ALL_CFLAGS = -I. $(CFLAGS)

default: all
all: linked_list stack binary_tree array
setup:
	@rm -f arch.zip dico.csv
	@curl -L -o arch.zip https://www.kaggle.com/api/v1/datasets/download/kartmaan/dictionnaire-francais
	@unzip arch.zip && rm arch.zip
linked_list:
	@$(CC) $(ALL_CFLAGS) linkedlist.c -o linkedlist
stack:
	@$(CC) $(ALL_CFLAGS) stack.c -o stack
binary_tree:
	@$(CC) $(ALL_CFLAGS) binary_tree.c -o binary_tree
array:
	@$(CC) $(ALL_CFLAGS) array.c -o array
prefix_tree:
	@$(CC) $(ALL_CFLAGS) prefix_tree.c -o prefix_tree
map:
	@$(CC) $(ALL_CFLAGS) map.c -o map
clean:
	@rm stack linkedlist binary_tree array prefix_tree map -f
