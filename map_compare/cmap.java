package map_compare;
import java.util.HashMap;
import java.util.Map;

class cmap {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<String, Integer>(8, 4);
        long start = System.nanoTime() / 1000;
        for (int i = 0; i < 9999; i++) {
            map.put(String.format("KEKW_%05d", i), 69+i);
        }
        long end = System.nanoTime() / 1000;
        System.out.println("Time taken: " + (end - start) + " us");
        System.out.println("Per operation: " + (end - start) / 9999 + " us");
    }
}