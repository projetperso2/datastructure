import time

def main():
    data = dict()
    start_time = time.time_ns()
    for i in range(0, 9999):
        data["KEKW_{0:05d}".format(i)] = 69+i
    end_time = time.time_ns()

    total_time_ns = end_time - start_time
    total_time_us = total_time_ns / 1000
    time_per_operation_us = total_time_us / 9999

    print(f"Total time: {total_time_us} microseconds")
    print(f"Time per operation: {time_per_operation_us} microseconds")

if __name__ == "__main__":
    main()