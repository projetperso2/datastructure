const process = require('process')

const data = new Map()
const start = process.hrtime.bigint()
for (let index = 0; index < 9999; index++) {
    data.set(`KEKW_${index.toString().padStart(5, '0')}`, 69+index);
}
const stop = process.hrtime.bigint()

const totalDuration = (stop - start) / 1000n; // in microseconds
const averageDuration = totalDuration / BigInt(9999); // in microseconds

console.log(`Total duration: ${totalDuration} microseconds`)
console.log(`Approximate duration of each operation: ${averageDuration} microseconds`)
