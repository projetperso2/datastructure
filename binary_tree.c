#include "assert.h"
#include "stdbool.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define STR_VALUE(arg) #arg;

const char *BT_DUMP_LEFT_STR = "│  ";
const char *BT_DUMP_RIGHT_STR = "   ";

/**
 *
 * Left : valeur inférieure,
 * Right : valeur supérieure
 *
 **/
struct node {
  struct node *left;
  struct node *right;
  struct node *parent;
  int value;
};
typedef struct node node_t;

// TODO à finir

node_t *bt_create(int root_value) {
  node_t *root = malloc(sizeof(node_t));
  root->value = root_value;
  return root;
}

void bt_free(node_t *root) {
  if (root->left != NULL) {
    bt_free(root->left);
    root->left = NULL;
  }
  if (root->right != NULL) {
    bt_free(root->right);
    root->right = NULL;
  }
  free(root);
}

void bt_insert(node_t *root, int value) {
  // left value of current node is undefined
  if (root->left == NULL) {
    node_t *left = malloc(sizeof(node_t));
    left->value = value;
    left->parent = root;
    root->left = left;
    return;
  }
  // The left value is greater than the value passed
  if (value < root->left->value) {
    // Recursive call, we insert the value to the left node
    bt_insert(root->left, value);
    return;
  }
  // here, the value must be in right

  // right value of current node is undefined
  if (root->right == NULL) {
    node_t *right = malloc(sizeof(node_t));
    right->value = value;
    right->parent = root;
    root->right = right;
    return;
  }

  // Last possiblity, insert the value at the right
  bt_insert(root->right, value);
}

node_t *bt_search(node_t *root, int value) {
  if (root == NULL || root->value == value) {
    return root;
  }
  if (value < root->value) {
    return bt_search(root->left, value);
  } else {
    return bt_search(root->right, value);
  }
}

bool bt_delete(node_t *root, int value) {
  node_t *correct_node = bt_search(root, value);
  // We can't delete a node that doesn't exist
  // We can't delete the root node
  if (correct_node == NULL || correct_node == root) {
    return false;
  }

  // Leaf node
  if (correct_node->left == NULL && correct_node->right == NULL) {
    // correct_node is a leaf node, so we need to get the parent
    if (correct_node->parent->left == correct_node) {
      correct_node->parent->left = NULL;
    } else if (correct_node->parent->right == correct_node) {
      correct_node->parent->right = NULL;
    }
    bt_free(correct_node);
    return true;
  }
  // TODO
}

void bt_dump(const node_t *root, int depth) {
  for (int i = 0; i < depth; i++) {
    putchar(' ');
  }
  printf("->%d\n", root->value);
  if (root->left != NULL) {
    bt_dump(root->left, depth + 1);
  }
  if (root->right != NULL) {
    bt_dump(root->right, depth + 1);
  }
}

void test_all_functions(node_t *root) {
  bt_insert(root, 2);
  bt_insert(root, 8);
  bt_insert(root, 1);
  bt_insert(root, 10);
  bt_insert(root, 12);
  bt_insert(root, 14);
  assert(root->left->value == 2);
  assert(root->right->value == 8);
  assert(root->left->left->value == 1);
  assert(root->right->left->value == 10);
  assert(root->right->right->value == 12);
  assert(root->right->right->left->value == 14);
  
  bt_dump(root, 0);
  bt_free(root);
  printf("It should SEGFAULT\n");
  bt_dump(root, 0);
}

int main(int argc, char const *argv[]) {
  node_t *root = bt_create(5);
  test_all_functions(root);
  return 0;
}
