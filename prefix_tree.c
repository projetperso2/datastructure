#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "assert.h"
#include "string.h"
#include "limits.h"
#include "sys/time.h"

#ifndef TRIE_STRUCTS
#define TRIE_STRUCTS

#define TRIE_ERROR_NONE 0
#define TRIE_ERROR_NOTFOUND 1
typedef unsigned short TRIE_ERROR;

#define TRIE_GROWTH_FACTOR 1.75
#define TRIE_DEFAULT_CHILDS_CAPACITY 5

typedef struct node
{
    struct node **childs;
    size_t child_count;
    size_t child_capacity;
    char key;
    int value;
} node_pt;

#endif

node_pt *pt_create()
{
    node_pt *root_node = (node_pt *)malloc(sizeof(node_pt));
    root_node->childs = (node_pt **)calloc(TRIE_DEFAULT_CHILDS_CAPACITY, sizeof(node_pt *));
    root_node->child_capacity = TRIE_DEFAULT_CHILDS_CAPACITY;
    root_node->child_count = 0;
    root_node->value = 0;
    root_node->key = '\0';
    return root_node;
}

void pt_free(node_pt *root_node)
{
    if (root_node == NULL)
    {
        return;
    }

    if (root_node->child_count != 0)
    {
        for (size_t i = 0; i < root_node->child_count; i++)
        {
            pt_free(root_node->childs[i]);
        }
        free(root_node->childs);
    }
    free(root_node);
}

void _pt_grow(node_pt *root_node)
{
    if (root_node->child_count < root_node->child_capacity)
        return;

    int new_len = root_node->child_count * (TRIE_GROWTH_FACTOR);
    if (root_node->child_count > INT_MAX / TRIE_GROWTH_FACTOR)
    {
        return;
    }
    // allocate a new block of memory
    node_pt **new_elements = (node_pt **)calloc(new_len, sizeof(node_pt *));
    // copy old value to new value
    memcpy(new_elements, root_node->childs, root_node->child_capacity);
    // free old memory
    free(root_node->childs);
    root_node->childs = new_elements;
    root_node->child_capacity = new_len;
}

void _pt_append_child(node_pt *root_node, node_pt *child_node)
{
    _pt_grow(root_node);

    root_node->childs[root_node->child_count] = child_node;
    root_node->child_count = root_node->child_count + 1;
}

void pt_append(node_pt *root_node, char *string, int value)
{

    node_pt *current_root_node = root_node;
    size_t str_size = strlen(string);

    for (size_t i = 0; i < str_size; i++)
    {

        // pour chaque children, s'il existe un avec la key, on l'utilise lui
        node_pt *child_node;
        char at = string[i];
        bool found = false;

        for (size_t c = 0; c < current_root_node->child_count; c++)
        {
            if (current_root_node->childs[c]->key == at)
            {
                found = true;
                child_node = current_root_node->childs[c];
                break; // exit child loop
            }
        }

        if (!found)
        {
            child_node = pt_create();
            child_node->key = string[i];
            // ajouter le child node  à la current_root_node, puis interchanger
            _pt_append_child(current_root_node, child_node);
        }
        current_root_node = child_node;

        if (i == str_size - 1)
        {
            current_root_node->value = value;
        }
    }
}

int pt_get(node_pt *root_node, char *string, TRIE_ERROR *result)
{
    if (string == NULL)
    {
        *result = TRIE_ERROR_NOTFOUND;
        return -1;
    }
    size_t len = strlen(string);
    node_pt *current_root_node = root_node;
    for (size_t i = 0; i < len; i++)
    {
        for (size_t c = 0; c < current_root_node->child_count; i++)
        {
            if (current_root_node->childs[c]->key == string[i])
            {
                current_root_node = current_root_node->childs[c];
                break;
            }
        }

        if (i == len - 1 && current_root_node->key == string[i])
        {
            return current_root_node->value;
        }
    }
    *result = TRIE_ERROR_NOTFOUND;
    return -1;
}

void _pt_print_depth(node_pt *root_node, bool compact, int depth)
{
    char *prefix = malloc(2 * depth + depth);
    int c_depth = depth;
    while (c_depth--)
    {
        strcat(prefix, " ");
    }
    if (compact)
    {
        printf("%s\\ \"%c\"->%d\n", prefix, root_node->key, root_node->value);
    }
    else
    {
        printf("%s\\{len=%ld,cap=%ld,val=%d,char='%c'}\n", prefix, root_node->child_count, root_node->child_capacity, root_node->value, root_node->key);
    }

    for (size_t i = 0; i < root_node->child_count; i++)
    {
        _pt_print_depth(root_node->childs[i], compact, depth + 1);
    }
}

void pt_print_debug(node_pt *root_node)
{
    _pt_print_depth(root_node, false, 0);
}

void pt_print(node_pt *root_node)
{
    _pt_print_depth(root_node, true, 0);
}

int main()
{
    struct timeval stop, start;
    gettimeofday(&start, NULL);
    node_pt *trie;
    trie = pt_create();

    pt_append(trie, "router", 1);
    pt_append(trie, "golmon", 42);
    pt_append(trie, "golem", 152);
    pt_append(trie, "rotor", 69420);

    TRIE_ERROR err;
    err = TRIE_ERROR_NONE;

    int val = pt_get(trie, "router", &err);
    if (err != TRIE_ERROR_NONE)
    {
        printf("Erreur pt_get(router) (%d) \n", err);
        exit(1);
    }
    printf("Valeur pour pt_get(router) = %d\n", val);

    pt_get(trie, "monkey", &err);
    if (err == TRIE_ERROR_NONE)
    {
        printf("Valeur trouvé sur monkey (impossible)\n");
        exit(1);
    }
    printf("Valeur pour pt_get(monkey) = NULL\n");

    pt_print(trie);
    pt_print_debug(trie);

    pt_free(trie);
    gettimeofday(&stop, NULL);
    printf("%lu us (10-6)\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

    printf("Should segfault\n");
    pt_print(trie);
}