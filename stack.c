#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "assert.h"
#include "sys/time.h"

#define st_is_empty(stack) stack->top == -1
#define st_capacity(stack) stack->capacity
#define st_peek(stack) stack->array[stack->top]
#define STACK_LOAD 10

struct stack
{
    int *array;
    int top;
    long capacity;
};
typedef struct stack stack_t;

void st_empty_stack_err(stack_t *stack)
{
    fprintf(stderr, "Stack is empty {top=%d, capacity=%ld}\n",
            stack->top,
            stack->capacity);
    exit(-1);
}

stack_t *st_create(int initial_capacity)
{
    stack_t *stack = malloc(sizeof(stack_t));
    stack->capacity = initial_capacity;
    stack->top = -1;
    stack->array = calloc(sizeof(int), initial_capacity);
    return stack;
}

void st_push(stack_t *stack, int value)
{
    // check capacity
    bool check = stack->top >= stack->capacity - 1;
    if (check)
    {
        // realloc the block of memory for the array
        int *new_array = realloc(stack->array, STACK_LOAD);
        stack->array = new_array;
        stack->capacity = stack->capacity + STACK_LOAD;
    }
    stack->top = stack->top + 1;
    stack->array[stack->top] = value;
}

void st_dump(stack_t *stack)
{
    printf("stack: {\n \tcapacity:%ld,\n\ttop: %d,\n\ttop value: %d,\n\tarray:%p\n}\n",
           stack->capacity,
           stack->top,
           stack->array[stack->top],
           stack->array);
}

int st_pop(stack_t *stack)
{
    // check for top
    if (st_is_empty(stack))
    {
        st_empty_stack_err(stack);
    }
    else
    {
        int top = stack->top;
        int val = stack->array[--top];
        stack->top = top;
        return val;
    }
    return -69420;
}

void st_free(stack_t *stack)
{
    free(stack->array);
    stack->array = NULL;
    free(stack);
    stack = NULL;
}

void test_all_functions(stack_t *stack)
{

    struct timeval stop, start;
    gettimeofday(&start, NULL);
    size_t iterations = 10000000;
    for (size_t i = 0; i < iterations; i++)
    {
        st_push(stack, 69);
        st_pop(stack);
    }

    gettimeofday(&stop, NULL);
    suseconds_t total_time = (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec;

    printf("%lu us (10-6), %.2f us per iteration\n", total_time, ((double)total_time) / ((double)iterations));
}

int main(int argc, char const *argv[])
{
    stack_t *stack = st_create(5);
    test_all_functions(stack);
    return 0;
}
