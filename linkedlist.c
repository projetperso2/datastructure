#include "stdio.h"
#include "stdlib.h"
#include "assert.h"

#define ll_get_tail(list) list->tail->value
#define ll_get_head(list) list->head->value

struct node
{
  struct node* next;
  int value;
};
typedef struct node node_t;

struct linked_list{
  node_t* head;
  node_t* tail;
  size_t size;
};
typedef struct linked_list linked_list_t;

typedef void (*for_each_t)(node_t*);
typedef void (*for_each_i_t)(int, node_t*);

linked_list_t* ll_create(){
  return malloc(sizeof(linked_list_t));
}

void ll_err_out_of_bound(linked_list_t* list, int index){
  fprintf(stderr, "Index >= size (size=%ld, index=%d)\n", list->size, index);
  exit(-1);
}

/**
 * @brief Insert a value at the beggining of the linked list
 * 
 * @param list the linked list
 * @param value the value to add
 */
void ll_insert_head(linked_list_t* list, int value){
  node_t *node = malloc(sizeof(node_t));
  node->value = value;
  if(list->head == NULL){
    // The list is empty
    list->head = node;
    list->tail = node;
    list->size = 1;
  }else{
    // There is only one element in the list
    node->next = list->head;
    list->head = node;
    list->size = list->size+1;
  }
}

/**
 * @brief Insert a value at the end of the linked list
 * 
 * @param list the linked list
 * @param value the value to add
 */
void ll_insert_tail(linked_list_t* list, int value){
  node_t *node = malloc(sizeof(node_t));
  node->value = value;
  if(list->head == NULL){
    // The list is empty
    list->head = node;
    list->tail = node;
    list->size = 1;
  }else{
    list->tail->next = node;
    list->tail = node;
    list->size = list->size+1;
  }
}

/**
 * @brief remove an element from the linked list
 * 
 * @param list the list
 * @param index the index of the removed element
 */
void ll_remove(linked_list_t* list, int index){
  if(index >= list->size){
    ll_err_out_of_bound(list, index);
  }

  node_t* toRemove = list->head;
  node_t* previous = NULL;
  for(int i = 0; i < index; i++){
    previous = toRemove;
    toRemove = toRemove->next;
  }
  if(toRemove == list->head){
    if(list->size == 1){
      list->head = NULL;
      list->tail = NULL;
      list->size = 0;
    }else{
      list->head = toRemove->next;
    }
  }else{
    previous->next = toRemove->next;
  }
  free(toRemove);
}

/**
 * @brief Insert an element at the given position
 * 
 * @param list The linked list
 * @param index The index to insert to
 * @param value The value to insert
 */
void ll_insert(linked_list_t* list, int index, int value){
  if(index >= list->size){
    ll_err_out_of_bound(list, index);
  }

  if(index == 0){
    ll_insert_head(list, value);
  }

  node_t* cursor = list->head;
  for(int i = 0; i < index-1; i++){
    cursor = cursor->next;
  }
  node_t* to_insert = malloc(sizeof(node_t));
  to_insert->value = value;
  to_insert->next = cursor->next;
  cursor->next = to_insert;
}

/**
 * @brief Return the value at a given index
 * 
 * @param list The linked list
 * @param index The index
 * @return The value at that given index
 */
int ll_get(linked_list_t* list, int index){
  
  if(index >= list->size){
    ll_err_out_of_bound(list, index);
  }
  
  if(index == 0){
    return list->head->value;
  }
  
  node_t* cursor = list->head;
  for(int i = 0; i < index; i++){
    cursor = cursor->next;
  }

  return cursor->value;
}

/**
 * @brief Set the value of the node at the current index
 * 
 * @param list The linked list
 * @param index the index
 * @param value the new value
 */
void ll_set(linked_list_t* list, int index, int value){
  if(index >= list->size){
    ll_err_out_of_bound(list, index);
  }

  if(index == 0){
    ll_insert_head(list, value);
  }

  node_t* cursor = list->head;
  for(int i = 0; i < index; i++){
    cursor = cursor->next;
  }
  cursor->value = value;
}

/**
 * @brief print the list to the console
 * 
 * @param list The linked list
 */
void ll_dump(linked_list_t* list){
  printf("list={");
  if(list->size == 0){
    printf("empty}\n");
    return;
  }
  node_t* cursor = list->head;
  size_t index = 0;
  while(1){
    printf(" %ld:%d ", index, cursor->value);
    cursor = cursor->next;
    if(cursor != NULL){
      printf("->");
      index++;
    }else{
      printf("}\n");
      break;
    }
  }
}

/**
 * @brief Free all the malloc pointers of the linked list
 * 
 * @param list The linked list
 */
void ll_free(linked_list_t* list){
  node_t* cursor = list->head;
  node_t* temp;
  while(1){
    temp = cursor->next;
    if(temp == NULL){
      break;
    }
    free(cursor);
    cursor = temp;
  }
  free(cursor);
  free(list);
  list = NULL;
  cursor = NULL;
}

void ll_for_each(linked_list_t* list, for_each_t for_each_func){
  for(node_t* cursor = list->head; cursor != NULL; cursor = cursor->next){
    for_each_func(cursor);
  }
}

void ll_for_each_i(linked_list_t* list, for_each_i_t for_each_func){
  int index = 0;
  for(node_t* cursor = list->head; cursor != NULL; cursor = cursor->next){
    for_each_func(index++, cursor);
  }
}

void for_each_print(node_t* node){
  printf("=> %d <=\n", node->value);
}

void for_each_print_i(int index, node_t* node){
  printf("-> index=%d, value=%d\n", index, node->value);
}

void test_all_functions(linked_list_t* list){
  ll_insert_tail(list, 5);
  ll_insert_tail(list, 6);
  ll_insert_tail(list, 8);
  ll_insert(list, 2, 7); // Insert at index 2, value 7

  assert(ll_get(list, 2) == 7);
  assert(ll_get_tail(list) == 8);
  
  ll_insert_head(list, 4);
  
  assert(ll_get_head(list) == 4);
  assert(ll_get(list, 1) == 5);
  
  ll_remove(list, 1);
  
  assert(ll_get(list, 0) == 4);
  assert(ll_get(list, 1) == 6);
  
  ll_dump(list);
  ll_free(list);
  
  printf("It should SEGFAULT\n");
  
  ll_dump(list);
}

int main(int argc, char const *argv[])
{
  linked_list_t* linked_list = ll_create();
  test_all_functions(linked_list);
  // for (size_t i = 0; i < 10; i++)
  // {
  //   ll_insert_tail(linked_list, i);
  // }
  
  // // Iterate 
  // ll_for_each(linked_list, &for_each_print);
  // ll_dump(linked_list);

  return 0;
}
