# Structure de données en C

I7 8700k 4.2GHz 6c/12th, 32Gram, Ubuntu 22.04 WSL2 8Gram allocated

## Stack

- 10 000 000 insertion

| Language    | Total Time (us) | Time per Iteration (us) |
| ----------- | --------------- | ----------------------- |
| C           | 20120           | 0.002                   |
| JS (bun)    | 30898           | 0.003                   |
| Java        | 189870          | 0.01                    |
| Python 3.10 | 850057          | 0.08                    |

- \* Chaque string est un malloc
- \*\* Chaque string est buffer statique

## Map

- 9999 insertion
- Init Map capacity: 8
- Growth factor: 4

| Language    | Total Time (us) | Time per Iteration (us) |
| ----------- | --------------- | ----------------------- |
| C \*\*      | 806             | 0.08                    |
| JS (bun)    | 5108            | 0.51                    |
| Python 3.10 | 6984            | 0.69                    |
| C \*        | 17546           | 1.75                    |
| Java        | 56838           | 5                       |

- \* Chaque string est un malloc
- \*\* Chaque string est buffer statique
