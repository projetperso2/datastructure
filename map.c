#include "stdio.h"
#include "stdlib.h"
#include "assert.h"
#include "string.h"
#include "limits.h"
#include "sys/time.h"

#ifndef MAP_DEFAULT_CAPACITY
#define MAP_DEFAULT_CAPACITY 8
#endif

#ifndef MAP_GROWTH_FACTOR
#define MAP_GROWTH_FACTOR 4
#endif

#ifndef MAP_EXIT_CODE
#define MAP_EXIT_CODE

#define MAP_GROWTH_INT_OVERFLOW 1
#define MAP_GET_WRONG_KEY 2
#endif

unsigned long _hash(char *str)
{
    unsigned long hash = 5381;
    int c;

    while ((c = *str++))
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}

typedef struct node
{
    char *key;
    int value;
} node_t;

typedef struct map
{
    node_t **data;
    size_t capacity;
    size_t len;
} map_t;

unsigned long _m_index(map_t *map, char *key)
{
    return _hash(key) % map->capacity;
}

void _m_grow_force(map_t *map)
{
    size_t new_capacity = map->capacity * (MAP_GROWTH_FACTOR);
    if (map->len > INT_MAX / MAP_GROWTH_FACTOR)
    {
        exit(MAP_GROWTH_INT_OVERFLOW);
        return;
    }
    // allocate a new block of memory
    node_t **new_elements = (node_t **)calloc(new_capacity, sizeof(node_t *));
    // rearange all keys
    for (size_t i = 0; i < map->capacity; i++)
    {
        if (map->data[i] != NULL)
        {
            unsigned long new_index = _hash(map->data[i]->key) % new_capacity;
            while (new_elements[new_index] != NULL)
            {
                new_index = (new_index + 1) % new_capacity;
            }
            new_elements[new_index] = map->data[i];
        }
    }
    // free old memory
    free(map->data);
    map->data = new_elements;
    map->capacity = new_capacity;
}

void _m_grow(map_t *map)
{
    if (map->len < map->capacity)
        return;
    _m_grow_force(map);
}

map_t *m_create()
{
    map_t *map = (map_t *)malloc(sizeof(map_t));
    map->data = (node_t **)calloc(MAP_DEFAULT_CAPACITY, sizeof(node_t *));
    memset(map->data, 0, MAP_DEFAULT_CAPACITY * sizeof(node_t *));
    map->capacity = MAP_DEFAULT_CAPACITY;
    map->len = 0;
    return map;
}

void m_free(map_t *map){
    for(size_t i = 0; i < map->capacity; i++){
        if(map->data[i] != NULL){
            free(map->data[i]);
        }
    }
    map->len = 0;
    map->capacity = 0;
    map->data = NULL;
}

void m_put(map_t *map, char *key, int val)
{
    int index = _m_index(map, key);
    if (map->data[index] == NULL)
    {
        map->data[index] = (node_t *)malloc(sizeof(node_t));
        map->data[index]->key = key;
        map->data[index]->value = val;
        map->len = map->len + 1;
        return;
    }
    if (strcmp(map->data[index]->key, key) == 0)
    {
        map->data[index]->value = val;
        return;
    }
    // Collision
    if (map->len < map->capacity - 1)
    {
        while (map->data[index] != NULL)
        {
            index = (index + 1) % map->capacity;
        }
        map->data[index] = (node_t *)malloc(sizeof(node_t));
        map->data[index]->key = key;
        map->data[index]->value = val;
        map->len = map->len + 1;
        return;
    }
    _m_grow_force(map);
    m_put(map, key, val);
}

int m_get(map_t *map, char *key)
{
    int index = _m_index(map, key);
    if (map->data[index] == NULL)
    {
        return -1;
    }
    if (strcmp(map->data[index]->key, key) != 0)
    {
        // Collision
        while (map->data[index] != NULL && (strcmp(map->data[index]->key, key) != 0))
        {
            index = (index + 1) % map->capacity;
        }
    }
    return map->data[index]->value;
}

int m_len(map_t *map)
{
    return map->len;
}

void m_print(map_t *map)
{
    printf("{len=%ld, cap=%ld}\n", map->len, map->capacity);
    for (size_t i = 0; i < map->capacity; i++)
    {
        if (map->data[i] != NULL)
        {
            printf("  - %s => %d\n", map->data[i]->key, map->data[i]->value);
        }
    }
}

int main()
{

    struct timeval stop, start;
    gettimeofday(&start, NULL);
    map_t *map;
    map = m_create();

    for(size_t i = 0; i < 9999; i++){
        char buffer[12];
        sprintf(buffer, "KEKW_%04ld", i);
        m_put(map, buffer, 69+i);
    }


    gettimeofday(&stop, NULL);
    suseconds_t total_time = (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec;
    
    m_free(map);
    printf("%lu us (10-6), %.2f us per iteration\n", total_time, ((double) total_time)/9999.0);
    printf("Should segfault\n");
    m_print(map);
}